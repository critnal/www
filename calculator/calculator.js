var inputString; // Takes the text input
var masterArray; // Stores the complete array of numbers and operators
var answer; // The final number to be displayed to the user



$(document).ready(function()
{
	$("#Calculate").click(function()
	{
		inputString = $("#Input").val();
		masterArray = strToArray(inputString);
		
		if ( !isValidEquation(masterArray) )
		{
			alert("Equation is not valid");
			return;
		}
		
		answer = operate(masterArray);
		displayAnswer(answer);
	});
});



function strToArray(string)
{
	var tempStrArray; // Holds input in initial string format
	var tempStr = ""; // Holds each string as it is extracted from tempStrArray
	var tempNum; // Holds each number to be added to tempIntArray
	var tempNumArray = new Array(); // Holds array of numbers and operators
	
	// Remove all spaces so they don't have to be dealt with later
	tempStrArray = string.replace(/ /g, "");
	
	for (var i = 0; i < tempStrArray.length; i++)
	{	
		if ( isNumber(tempStrArray[i]) )
		{
			tempStr += tempStrArray[i];
			
			if ( i == tempStrArray.length - 1 )
			{
				tempNum = parseFloat(tempStr);
				tempNumArray.push(tempNum);
			}
		}			
		else
		{
			if ( tempStr.length > 0)
			{
				tempNum = parseFloat(tempStr);
				tempNumArray.push(tempNum);
				tempStr = "";
			}
			tempNumArray.push(tempStrArray[i]);			
		}
	}
	
	return tempNumArray;
}



function isNumber(string)
{
	switch (string)
	{
		case "." :
		case "0" :
		case "1" :
		case "2" :
		case "3" :
		case "4" :
		case "5" :
		case "6" :
		case "7" :
		case "8" :
		case "9" :
			return true;
		
		default :
			return false;
	}
}



function isValidEquation(array)
{
	// var openBracketCount;
	// var closeBracketCount;
	
	// // Ensure first character is a number
	// if ( !isFinite( array[0] ) )
		// return false;
	
	// // Ensure last character is a number
	// if ( !isFinite( array[array.length - 1] ) )
		// return false;
	
	// // Ensure open and close bracket counts match
	// for (var i = 0; i < array.length; i++)
	// {
		
	// {
	return true;
}



function operate(array)
{
	var arrayLength = array.length;
	var i = 0;	
	var result = 0;
	
	
	
	// simplify brackets
	while ( (i < arrayLength) && (arrayLength > 1) )
	{
		if ( array[i] == "(" )
		{
			var bracketCount = 1;
			var startIndex = i;
			var endIndex = 0;
			var numberToRemove = 0;
			var subArray;
			
			while ( bracketCount > 0 )
			{
				i++;
				
				if ( array[i] == "(" )
					bracketCount++;
				else if ( array[i] == ")" )
					bracketCount--;
			}
			endIndex = i + 1;
			numberToRemove = endIndex - startIndex;			
			subArray = array.slice( startIndex + 1, endIndex - 1);
			
			result = operate( subArray );
			array.splice( startIndex, numberToRemove, result );
			
			i = startIndex; // Make sure the function continues to traverse the array from the startIndex
		}
		else
			i++;
		
		arrayLength = array.length;
	}	
	i = 0;
	
	
	
	
	
	// powers
	while ( (i < arrayLength) && (arrayLength > 1) )
	{
		if ( array[i] == "^" )
		{
			result = Math.pow( array[i - 1], array[i + 1] );
			array.splice( i - 1, 3, result);
		}
		else
			i++;
			
		arrayLength = array.length;
	}
	i = 0;
	
	
	
	
	
	// multiplication/division
	while ( (i < arrayLength) && (arrayLength > 1) )
	{
		if ( array[i] == "*" )
		{
			result = array[i - 1] * array[i + 1];
			array.splice( i - 1, 3, result);
		}
		else if ( array[i] == "/" )
		{
			result = array[i - 1] / array[i + 1];
			array.splice( i - 1, 3, result);
		}
		else
			i++;
			
		arrayLength = array.length;
	}
	i = 0;
	
	
	
	
	
	// addition/subtraction
	while ( (i < arrayLength) && (arrayLength > 1) )
	{
		if ( array[i] == "+" )
		{
			result = array[i - 1] + array[i + 1];
			array.splice( i - 1, 3, result);
		}
		else if ( array[i] == "-" )
		{
			result = array[i - 1] - array[i + 1];
			array.splice( i - 1, 3, result);
		}
		else
			i++;
			
		arrayLength = array.length;	
	}
	
	return array[0]; // The only remaining element should be the entire equation in simplified form
}



function displayAnswer(answer)
{
	$("#Answer").text(answer);
}